<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">

<jsp:include page="header.jsp"></jsp:include>

<body id="page-top">
<div id="wrapper">
<jsp:include page="sidebar.jsp"></jsp:include>
  <div id="content-wrapper" class="d-flex flex-column">
    <div id="content">
      <jsp:include page="topbar.jsp"></jsp:include>
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Gestion des Conventions</h1>
          <p class="mb-4"><a class="btn btn-primary" href="/addConvention">Ajouter</a></p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Objet</th>
                      <th>Type</th>
                      <th>Date Edition</th>
                      <th>Date Expération</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>Objet</th>
                      <th>Type</th>
                      <th>Date Edition</th>
                      <th>Date Expération</th>
                    </tr>
                  </tfoot>
                  <c:forEach var="convention" items="${listConv}">
                  	<tbody>
                  		<tr>
                  			<td><c:out value="${convention.id}"></c:out></td>
                          <td><c:out value="${convention.objet}"></c:out></td>
                          <td><c:out value="${convention.type}"></c:out></td>
                          <td><c:out value="${convention.date_edition}"></c:out></td>
                          <td><c:out value="${convention.date_exp}"></c:out></td>

                        </tr>
                  	</tbody>
                  </c:forEach>

                </table>
              </div>
            </div>
          </div>

        </div>
    </div>
    <jsp:include page="footer.jsp"></jsp:include>
  </div>
</div>
<jsp:include page="scripts.jsp"></jsp:include>
</body>
</html>