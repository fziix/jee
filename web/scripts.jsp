<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- Bootstrap core JavaScript-->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="assets/js/sb-admin-2.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    } );
</script>
<!-- Page level plugins -->
<script src="assets/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/js/main.js"></script>
<!-- Page level custom scripts --><script>

</script>
<script>
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })
</script>
<script>
    let btn=document.getElementById("btn-ajt");
    let form=document.getElementById("form");
    btn.addEventListener("click",function (event) {
        form.submit();
    })

</script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
    let range=document.getElementById("tayeb");
    let p2=document.getElementById("P2");
    let p3=document.getElementById("P3");
    let p4=document.getElementById("P4");
    setParticipants(1);
    range.addEventListener("change",function(event){
        setParticipants(event.target.value)

    });

    function setParticipants(number){
        switch(parseInt(number)){
            case 1:
                p2.style.display="none"
                p3.style.display="none";
                p4.style.display="none";
                break;
            case 2:
                p2.style.display="block"
                p3.style.display="none";
                p4.style.display="none";
                break;
            case 3:
                p2.style.display="block"
                p3.style.display="block";
                p4.style.display="none";
                break;
            case 4:
                p2.style.display="block"
                p3.style.display="block";
                p4.style.display="block";
                break;
        }
    }
</script>

<script src="assets/js/demo/datatables-demo.js"></script>