package iit.entity;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
public class userCreate {
    public static void create(){
        SessionFactory sessionFactory = new Configuration().
                configure("hibernate.cfg.xml").
                addAnnotatedClass(Utilisateur.class).
                buildSessionFactory();


        //create a Session
        Session session = sessionFactory.getCurrentSession();
        try{

            System.out.println("Creating a new Student object...");

            //create the Student object
            Utilisateur student = new Utilisateur("slim@slim.tn", "123123","admin");

            //start a transaction
            session.beginTransaction();

            //Save the Student object to the database
            session.save(student);

            //commit the transaction
            session.getTransaction().commit();

        }finally{

            sessionFactory.close();
        }
    }
}
