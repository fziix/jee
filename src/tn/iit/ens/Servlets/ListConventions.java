package tn.iit.ens.Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import tn.iit.ens.Entities.Convention;
import tn.iit.ens.Entities.ConventionParticipant;
import tn.iit.ens.Entities.Participant;


/**
 * Servlet implementation class listConventions
 */
@WebServlet("/listConventions")
public class ListConventions extends HttpServlet {
    public static final String VUE = "/conventionList.jsp";

	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListConventions() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        SessionFactory sessionFactory = new Configuration().
                configure("hibernate.cfg.xml")
                .addAnnotatedClass(Convention.class)
                .addAnnotatedClass(Participant.class)
                .addAnnotatedClass(ConventionParticipant.class)
                .buildSessionFactory();
        //create a Session
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        List<Convention> listConv=Convention.findAllConventions(session);
        session.getTransaction().commit();
        request.setAttribute("listConv",listConv );
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
        sessionFactory.close();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
