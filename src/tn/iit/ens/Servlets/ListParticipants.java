package tn.iit.ens.Servlets;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import tn.iit.ens.Entities.Participant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/ListParticipants")
public class ListParticipants extends HttpServlet {
    public static final String VUE = "/participantList.jsp";
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SessionFactory sessionFactory = new Configuration().
                configure("hibernate.cfg.xml")
                .addAnnotatedClass(Participant.class)
                .buildSessionFactory();
        //create a Session
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        List<Participant> listParts=Participant.findAllParticipants(session);
        session.getTransaction().commit();
        request.setAttribute("listParticipants",listParts );
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
        sessionFactory.close();
    }
}
