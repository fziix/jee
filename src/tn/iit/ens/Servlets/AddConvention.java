package tn.iit.ens.Servlets;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import tn.iit.ens.Entities.Convention;
import tn.iit.ens.Entities.ConventionParticipant;
import tn.iit.ens.Entities.Participant;
import tn.iit.ens.Forms.AddConventionForm;


/**
 * Servlet implementation class AddConvention
 */
@WebServlet("/addConvention")
public class AddConvention extends HttpServlet {
    public static final String VUE = "/addConvention.jsp";
    public static final String ATT_CONVENTION = "convention";
    public static final String ATT_FORM = "form";
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddConvention() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
        AddConventionForm form = new AddConventionForm();
        Convention convention = form.addConvention( request );
        
        SessionFactory sessionFactory = new Configuration().
                configure("hibernate.cfg.xml")
                .addAnnotatedClass(Convention.class)
                .addAnnotatedClass(Participant.class)
                .addAnnotatedClass(ConventionParticipant.class)
                .buildSessionFactory();


        //create a Session
        Session session = sessionFactory.getCurrentSession();
        try{
         session.beginTransaction();

        Participant pa=new Participant("ZEZE");
        Participant pa2=new Participant("RRR");
        session.save(pa);
        session.save(pa2);
        Set subMenu1 = new HashSet();
        subMenu1.add(new ConventionParticipant(convention,pa));
        subMenu1.add(new ConventionParticipant(convention,pa2));
        convention.setConventionPartion(subMenu1);

        session.save(convention);

        session.getTransaction().commit();
        }
        catch(Exception e) {
        	
        }
        finally {
            sessionFactory.close();

        }
        request.setAttribute( ATT_FORM, form );
        request.setAttribute( ATT_CONVENTION, convention );
		doGet(request, response);
	}
	
	

}
