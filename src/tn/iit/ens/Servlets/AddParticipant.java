package tn.iit.ens.Servlets;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import tn.iit.ens.Entities.Participant;

import tn.iit.ens.Forms.AddParticipantForm;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@WebServlet("/addParticipant")
public class AddParticipant extends HttpServlet {
    public static final String VUE = "/addParticipant.jsp";
    public static final String ATT_PARTICIPANT = "participant";
    public static final String ATT_FORM = "form";
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddParticipant() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        AddParticipantForm form = new AddParticipantForm();
        Participant participant = form.addParticipant( request );

        SessionFactory sessionFactory = new Configuration().
                configure("hibernate.cfg.xml")
                .addAnnotatedClass(Participant.class)
                .addAnnotatedClass(Participant.class)
                .buildSessionFactory();


        //create a Session
        Session session = sessionFactory.getCurrentSession();
        try{
            session.beginTransaction();

            Participant pa=new Participant("SLIM");
            Participant pa2=new Participant("TAYEB");
            session.save(pa);
            session.save(pa2);
            Set subMenu1 = new HashSet();
            subMenu1.add(new Participant (participant,pa));
            subMenu1.add(new Participant (participant,pa2));
            participant.setParticipant(subMenu1);

            session.save(participant);

            session.getTransaction().commit();
        }
        catch(Exception e) {

        }
        finally {
            sessionFactory.close();

        }
        request.setAttribute( ATT_FORM, form );
        request.setAttribute( ATT_PARTICIPANT, participant );
        doGet(request, response);
    }



}