package tn.iit.ens.Forms;

import tn.iit.ens.Entities.Participant;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class AddParticipantForm {
    private static final String CHAMP_NOM  = "nom";
    private static final String CHAMP_PRENOM = "prenom";
    private static final String CHAMP_ADRESESEMAIL   = "adresseMail";
    private static final String CHAMP_NUMERO    = "numero";

    private String resultat;
    private Map<String, String> erreurs      = new HashMap<String, String>();

    public String getResultat() {
        return resultat;
    }

    public Map<String, String> getErreurs() {

        return erreurs;
    }
    public Participant addParticipant(HttpServletRequest request ) {
        String nom = getValeurChamp( request, CHAMP_NOM );
        String prenom= getValeurChamp( request, CHAMP_PRENOM
        );
        String adresseMail = getValeurChamp( request, CHAMP_ADRESESEMAIL );
        String numero = getValeurChamp( request, CHAMP_NUMERO );

        Participant convention = new Participant();

        try {
            validationString( nom );
        } catch ( Exception e ) {
            setErreur( CHAMP_NOM, e.getMessage() );
        }
        convention.setNom( nom );

        try {
            validationString(prenom);
        } catch ( Exception e ) {
            setErreur( CHAMP_PRENOM
                    , e.getMessage() );
        }
        convention.setPrenom(prenom);

        try {
            validationEmail( adresseMail );

        } catch ( Exception e ) {
            setErreur( CHAMP_ADRESESEMAIL, e.getMessage() );
        }
        convention.setAdresseMail(adresseMail);


        try {
            validationString( numero );
        } catch ( Exception e ) {
            setErreur( CHAMP_NUMERO, e.getMessage() );
        }
        convention.setNumero(numero);



        return convention;
    }

    private void validationString(String nom) {
    }

    private void validationNom( String nom ) throws Exception {
        if ( nom == null ) {

                throw new Exception( "Merci de remplir le champ nom" );
        }
    }
    private void validationPrenom( String prenom ) throws Exception {
        if ( prenom == null ) {

            throw new Exception( "Merci de remplir le champ prenom" );
        }
    }



    private void validationEmail( String email ) throws Exception {
        if ( email != null ) {
            if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new Exception( "Merci de saisir une adresse mail valide." );
            }
        } else {
            throw new Exception( "Merci de saisir une adresse mail." );
        }
    }
    private void validationNumero ( String numero ) throws Exception {
        if ( numero == null ) {

            throw new Exception( "Merci de remplir le champ numero" );
        }
    }



    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur.trim();
        }
    }
}