package tn.iit.ens.Forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import tn.iit.ens.Entities.Convention;
public class AddConventionForm {
    private static final String CHAMP_TYPE  = "type";
    private static final String CHAMP_DEDT  = "date_edition";
    private static final String CHAMP_OBJET   = "objet";
    private static final String CHAMP_DVIG    = "date_vigueur";
    private static final String CHAMP_DEXP ="date_exp";
    
    private String resultat;
    private Map<String, String> erreurs      = new HashMap<String, String>();

    public String getResultat() {
        return resultat;
    }

    public Map<String, String> getErreurs() {
    	
        return erreurs;
    }
    public Convention addConvention( HttpServletRequest request ) {
        String type = getValeurChamp( request, CHAMP_TYPE );
        String date_edition = getValeurChamp( request, CHAMP_DEDT );
        String objet = getValeurChamp( request, CHAMP_OBJET );
        String date_vigueur = getValeurChamp( request, CHAMP_DVIG );
        String date_exp = getValeurChamp( request, CHAMP_DEXP );

        Convention convention = new Convention();

        try {
            validationString( type );
        } catch ( Exception e ) {
            setErreur( CHAMP_TYPE, e.getMessage() );
        }
        convention.setType( type );
        
        try {
            validationString( date_edition );
        } catch ( Exception e ) {
            setErreur( CHAMP_DEDT, e.getMessage() );
        }
        convention.setDate_edition(date_edition);
        
        try {
            validationObjet( objet );
            
        } catch ( Exception e ) {
            setErreur( CHAMP_OBJET, e.getMessage() );
        }
        convention.setObjet(objet);
        
        
        try {
            validationString( date_vigueur );
        } catch ( Exception e ) {
            setErreur( CHAMP_DVIG, e.getMessage() );
        }
        convention.setDate_vigueur(date_vigueur);
        
        try {
            validationString( date_exp );
        } catch ( Exception e ) {
            setErreur( CHAMP_DEXP, e.getMessage() );
        }
        convention.setDate_exp(date_exp);


        return convention;
    }
    private void validationEmail( String email ) throws Exception {
        if ( email != null ) {
            if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new Exception( "Merci de saisir une adresse mail valide." );
            }
        } else {
            throw new Exception( "Merci de saisir une adresse mail." );
        }
    }

    private void validationMotsDePasse( String motDePasse, String confirmation ) throws Exception {
        if ( motDePasse != null && confirmation != null ) {
            if ( !motDePasse.equals( confirmation ) ) {
                throw new Exception( "Les mots de passe entr�s sont diff�rents, merci de les saisir � nouveau." );
            } else if ( motDePasse.length() < 3 ) {
                throw new Exception( "Les mots de passe doivent contenir au moins 3 caract�res." );
            }
        } else {
            throw new Exception( "Merci de saisir et confirmer votre mot de passe." );
        }
    }

    private void validationObjet( String obj ) throws Exception {
        if ( obj != null && obj.length() < 10 ) {
            throw new Exception( "L'objet doit contenir au moins 10 caract�res." );
        }
    }
    private void validationString(String obj) throws Exception{
    	if(obj!=null) {
    		throw new Exception("Invalid champ!!");
    	}
    }


    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur.trim();
        }
    }
}
