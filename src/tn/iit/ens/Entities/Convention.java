package tn.iit.ens.Entities;

import java.time.LocalDate;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;


import org.hibernate.Session;


@Entity
@Table(name = "convention")
public class Convention {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "convention_id")
    private int id;


    @Column(name = "type")
    private String type;

    @Column(name = "date_edition")
    private String date_edition;

    @Column(name = "objet")
    private String objet;

    @Column(name = "date_vigueur")
    private String date_vigueur;

    @Column(name = "date_exp")
    private String date_exp;

    @OneToMany(mappedBy = "convention", cascade = CascadeType.ALL)
    Set<ConventionParticipant> conventionPartion = new HashSet<>();


    public Set getConventionPartion() {
        return conventionPartion;
    }


    public Convention() {
        super();
    }


    public void setConventionPartion(Set conventionPartion) {
        this.conventionPartion = conventionPartion;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public String getDate_edition() {
        return date_edition;
    }


    public void setDate_edition(String date_edition) {
        this.date_edition = date_edition;
    }


    public String getObjet() {
        return objet;
    }


    public void setObjet(String objet) {
        this.objet = objet;
    }


    public String getDate_vigueur() {
        return date_vigueur;
    }


    public void setDate_vigueur(String date_vigueur) {
        this.date_vigueur = date_vigueur;
    }


    public String getDate_exp() {
        return date_exp;
    }


    public void setDate_exp(String date_exp) {
        this.date_exp = date_exp;
    }

    public Convention(String type, String date_edition, String objet, String date_vigueur, String date_exp) {
        super();
        this.type = type;
        this.date_edition = date_edition;
        this.objet = objet;
        this.date_vigueur = date_vigueur;
        this.date_exp = date_exp;
    }


    @Override
    public String toString() {
        return "ok";
    }

    public static List<Convention> findAllConventions(Session session) {
        return session.createQuery("SELECT a FROM Convention a", Convention.class).getResultList();
    }


}
