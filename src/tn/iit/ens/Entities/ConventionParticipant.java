package tn.iit.ens.Entities;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="conventionparticipant")
public class ConventionParticipant {
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
	
    
    @Column(name="date_signature")
    private String date_edition;
    
    @ManyToOne
    @JoinColumn(name ="convention_id")
    private Convention convention;
    
    @ManyToOne
    @JoinColumn(name ="participant_id")
    private Participant participant;

	public ConventionParticipant(String date_edition, Convention convention, Participant participant) {
		super();
		this.date_edition = date_edition;
		this.convention = convention;
		this.participant = participant;
	}

	public ConventionParticipant(Convention convention, Participant participant) {
		super();
		this.convention = convention;
		this.participant = participant;
	}

    
    
    




    

}
