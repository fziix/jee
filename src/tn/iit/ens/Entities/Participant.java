package tn.iit.ens.Entities;

import org.hibernate.Session;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="participant")
public class Participant {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "participant_id")
    private int id;



	@OneToMany(mappedBy="participant", cascade = CascadeType.ALL)
    private Set<ConventionParticipant> conventionParticipant = new HashSet<>();

	@Column(name="nom")
	private String nom;

	@Column(name = "prenom")
	private String prenom;

	public Participant() {
		super();
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresseMail() {
		return adresseMail;
	}

	public void setAdresseMail(String adresseMail) {
		this.adresseMail = adresseMail;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Column(name = "adresseMail")
	private String adresseMail;

	@Column(name = "numero")
	private String numero;

    public Participant(Participant participant, Participant pa) {
    	super();

    }

	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public Participant(String nom) {
		this.nom=nom;
	}
	

	public int getId() {
		return id;
	}


	public Set getConventionParticipant() {
		return conventionParticipant;
	}


	public void setConventionParticipant(Set conventionParticipant) {
		this.conventionParticipant = conventionParticipant;
	}

	public Participant(String nom, String prenom, String adresseMail, String numero) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.adresseMail = adresseMail;
		this.numero = numero;
	}




	public void setId(int id) {
		this.id = id;
	}

	public static List<Participant> findAllParticipants(Session session) {
		return session.createQuery("SELECT a FROM Participant a", Participant.class).getResultList();
	}

	public void setParticipant(Set subMenu1) {

	}
}
