<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="header.jsp"></jsp:include>

<body id="page-top">
<div id="wrapper">
    <jsp:include page="sidebar.jsp"></jsp:include>
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <jsp:include page="topbar.jsp"></jsp:include>
            <form method="post" action="/addConvention" id="form">
                <div class="container-fluid">
                    <h1 class="h3 mb-4 text-gray-800">Nouvelle Convention</h1>

                    <div class="row">

                        <div class="col-lg-6">

                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Dates</h6>
                                </div>
                                <div class="card-body">
                                    <p>Specifiez les dates de l'edition,d'entrée en vigueur et d'expiration</p>
                                    <div class="mb-2">
                                        <label class="font-weight-bold">La date d'edition</label>
                                    </div>
                                    <input type="date" class="form-control" name="date_edition">
                                    <div class="mt-4 mb-2">
                                        <label class="font-weight-bold">La date de son entrée en vigueur</label>
                                    </div>
                                    <input type="date" class="form-control " name="date_vigueur">
                                    <div class="mt-4 mb-2">
                                        <label class="font-weight-bold">La date de son expiration</label>
                                    </div>
                                    <input type="date" class="form-control " name="date_exp">
                                </div>
                            </div>
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Détail</h6>
                                </div>
                                <div class="card-body">
                                    <p>Spécifiez le type et l'objet du convention.</p>
                                    <div class="mb-2">
                                        <label class="font-weight-bold">Type</label>
                                    </div>
                                    <select class="form-control" name="type">
                                        <option></option>
                                        <option>Universitaire</option>
                                        <option>Industrielle nationale</option>
                                        <option>Industrielle</option>

                                    </select>
                                    <div class="mt-4 mb-2">
                                        <label class="font-weight-bold">Objet</label>
                                    </div>
                                    <textarea class="form-control" name="objet"
                                              placeholder="La description du convention..."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Participants</h6>
                                </div>
                                <div class="card-body">
                                    <p>Choisissez les participants (2 ou plus)</p>
                                    <div class="mb-2">
                                        <label class="font-weight-bold">Nombre De Participants</label>
                                    </div>
                                    <div class="range-slider">
                                        <input id="tayeb" class="range-slider__range" type="range" value="1" min="1"
                                               max="4"
                                               step="1">
                                        <span class="range-slider__value">0</span>
                                        <pre></pre>
                                        <p id="demo"></p>
                                    </div>


                                    <div id="P1" class="wrap-input100 validate-input bg1 rs1-wrap-input100"
                                         data-validate="Enter Your Email (e@a.x)">
                                        <div class="mb-2">
                                            <label class="font-weight-bold">Participant 1</label>
                                        </div>
                                        <input class="form-control" type="text" name="email"
                                               placeholder="Entrer le mail du Participant 1 ">
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                                data-target="#myModal">Détails
                                        </button>
                                    </div>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-contact100">
                                                <div class="wrap-contact100">
                                                    <form class="container">

                                                        <div class="wrap-input100 validate-input bg1" data-validate="Please Type Your Name">
                                                            <span class="label-input100">Nom Complet</span>
                                                            <input class="input100" type="text" name="name" placeholder="Enter Your Name">
                                                        </div>

                                                        <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Enter Your Email (e@a.x)">
                                                            <span class="label-input100">Adresse Mail</span>
                                                            <input class="input100" type="text" name="email" placeholder="Enter Your Email ">
                                                        </div>

                                                        <div class="wrap-input100 bg1 rs1-wrap-input100">
                                                            <span class="label-input100">Téléphone</span>
                                                            <input class="input100" type="text" name="phone" placeholder="Enter Number Phone">
                                                        </div>


                                                </div>


            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>

</div>
</div>

</div>
                                    <div id="P2" class="wrap-input100 validate-input bg1 rs1-wrap-input100"
                                         data-validate="Enter Your Email (e@a.x)">
                                        <div class="mt-4 mb-2">
                                            <label class="font-weight-bold">Participant 2</label>
                                        </div>
                                        <input class="form-control" type="text" name="email"
                                               placeholder="Entrer le mail du Participant 2">
                                    </div>
                                    <div id=P3 class="wrap-input100 validate-input bg1 rs1-wrap-input100"
                                         data-validate="Enter Your Email (e@a.x)">
                                        <div class="mt-4 mb-2">
                                            <label class="font-weight-bold">Participant 3</label>
                                        </div>
                                        <input class="form-control" type="text" name="email"
                                               placeholder="Entrer le mail du Participant 3">
                                    </div>
                                    <div id="P4" class="wrap-input100 validate-input bg1 rs1-wrap-input100"
                                         data-validate="Enter Your Email (e@a.x)">
                                        <div class="mt-4 mb-2">
                                            <label class="font-weight-bold">Participant 4</label>
                                        </div>
                                        <input class="form-control" type="text" name="email"
                                               placeholder="Entrer le mail du Participant 4">

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex justify-content-center">
                        <a href="#" class="btn btn-secondary btn-circle btn-lg">
                            <i class="fas fa-arrow-left"></i>
                        </a>
                        <a type="button" data-toggle="modal" data-target="#exampleModal"

                           class="btn btn-success btn-circle btn-lg text-white">
                            <i class="fas fa-check"></i>
                        </a>


                    </div>
                </div>
            </form>
        </div>
        <jsp:include page="footer.jsp"></jsp:include>
    </div>
</div>





<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Voulez-vous enregistrer les modifications?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-success" id="btn-ajt">Confirmer</button>
            </div>
        </div>
    </div>
</div>
<jsp:include page="scripts.jsp"></jsp:include>

</body>
</html>